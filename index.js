// console.log("Hello World!");

// [SECTION] JSON Object

/*
	- JSON stands for JavaScript Object Notation
		- it is also used in other programming languages
		- Core JavaScript has a buit JSON objects that contains methods for passing JSON objects
		-JSON is used for serializing different data types into bytes


*/

// JSON Object
/*
	JSON also usses the "key/value pairs" just like the object properties in JavaScript
	- Key/property names should be enclosed with DOUBLE QUOTES

	Syntax:
		{
			"propertyA" : "valueA",	
			"propertyB" : "valueB"

		}
*/

/*
{
	"city" : "Quezon City",
	"province" : "Metro Manila",
	"country" : "Philippines"

}
*/


// JSON Arrays
// Arrays in JSON are almost same as arrays in JS
// Arrays in JSON Object
/*
	"ciries" : [
		{
			"city" : "Quezon City",
			"province" : "Metro Manila",
			"country": "Philippines"
		},
		{
			"city" : "Manila City",
			"province" : "Metro Manila",
			"country" : "Philippines"

		},
		{
			"city" : "Makati City",
			"province": "Metro Manila",
			"country" : "Philippines"

		}
	]

*/


// [SECTION] JSON Methods

// the "JSON Object" contains methods for parsing and converting data in stringified JSON
// JSON data is sent or received in text-only(String) format

// Converting data intro Stringified JSON

//Ex. 1
let batchesArr = [
		{
			batchName: 230,
			schedule: "PartTime"
		},
		{
			batchName: 240,
			schedule: "Full Time"
		}

	];

console.log(batchesArr);

console.log("Result from stringified method: ");

/*Syntax : JSON.stringify(arrays/objects/);*/
console.log(JSON.stringify(batchesArr));

//Ex.2
let data = JSON.stringify(
		{
			name: "John",
			age: 31,
			address: {
				city: "Manila",
				country: "Philippines"
		}
		
		}
	)
console.log(data);

//User details

/*let firstName = prompt("Enter your first name:");
let lastName = prompt("Enter your last name: ");
let email = prompt("Enter your email: ");
let password = prompt("Enter your password: ");

let userDetails = JSON.stringify(
		{
			firstName: firstName,
			lastName: lastName,
			email: email,
			password: password
		}
	);
console.log(userDetails);*/

// [SECTION] Converting Stringified JSON into JavaScriptsObjects:

let batchesJSON = `[
	{
		"batchName": 230,
		"schedule": "PartTime"
	},
	{
		"batchName": 240,
		"schedule": "Full Time"
	}
]`;

console.log("batchesJSON content: ");
console.log(batchesJSON);

console.log("Result from parse method: ");
let parseBatches = JSON.parse(batchesJSON);
console.log(parseBatches);
console.log(parseBatches[0].batchName);


let stringifiedObject = `
	{
		"name" : "John",
		"age" : 31,
		"address" : {
			"city" : "Manila",
			"country" : "Philippines"
		}
	}

`
console.log(stringifiedObject);

let regularObject = JSON.parse(stringifiedObject);
console.log(regularObject);

//or

// console.log(JSON.parse(stringifiedObject));


